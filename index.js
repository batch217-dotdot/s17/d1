// console.log("Hello World");

// Setion - Functions

function printName() {
	console.log("My name is John");
}
printName();

function declaredFunction() {
	console.log("Hello World from declaredFunction()");
}
declaredFunction();

// Function Expression
// function stored in a variable.

let variableFunction = function() {
	console.log("Hello Again!");
}
variableFunction();

let funcExpression = function funcName() {
	console.log("Hello from the other side.");
}
funcExpression();

// Reassigning funcExpression() value
funcExpression = function() {
	console.log("Updated funcExpression");
}
funcExpression();

// Reassigning declaredFunction() value
declaredFunction = function() {
	console.log("Updated declaredFunction");
}
declaredFunction();

// Re-assigning function declared with const
const constantFunc = function() {
	console.log("Initialized with const!")
}
constantFunc();

// Re-assignment of a const function
/*constantFunc = function() {
	console.log("Can we re-assign it?");
}
constantFunc();*/

// Section - Function scoping

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

let globalScope = "Mr. Worldwide";
console.log(globalScope);

// Function scoping

function showNames() {
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);
}
showNames();

// Nested function

function myNewFunction() {
	let name = "Jane";

	function nestedFunction() {
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();

// Function and global scoped variable

// Global scoped variable


let globalName = "Alendro";

function myNewFunction2() {
	let nameInside = "Renz";
	console.log(globalName);
}
myNewFunction2();

// Section - Using alert()

// alert("Hello World!"); // This will be executed immediately

function showAlert() {
	alert("Hello User!");
}
// showAlert();

console.log("I will only log in the console when alert is dismissed.")

// Section - Using prompt()

// let samplePrompt = prompt("Enter yout name.");
// console.log("Hello, " + samplePrompt);
// console.log(typeof(samplePrompt));

// prompt() can be used to gather user input
// prompt() it can be run immediately

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}
printWelcomeMessage();

// Section - Function Naming Condition

function getCourse() {
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}
getCourse();

// avoid generic names to avoid confusion within your code
function get() {
	let name = "Jamie";
	console.log(name);
}
get();

// avoid pointless and inappropriate function names
function foo() {
	console.log(25%5);
}
foo();

// Name your functions in small caps. Follow camelCase when naming variables and functions

// camelcase --> myNameIsRussel
// snake_case --> my_name_is_russel
// kebab-case --> my-name-is-russel

function displayCarInfo() {
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1, 500, 000");
}
displayCarInfo();